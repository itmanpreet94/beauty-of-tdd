<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function create_user_in_database_through_factory()
    {
        $user = factory(User::class)->create([]);
        $this->assertDatabaseHas('users', $user->toArray());
    }
   
     /** @test */

    public function check_validation_for_user()
    {
        $data = [
            'email' => '',
            'name' => '',
            'password' => '',
        ];

        $response = $this->postJson(route('user.store'), $data);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            "message" ,
            "errors"
        ]);
    }

     /** @test */

     public function create_user_through_api()
     {
         $data = [
             'email' => 'xyz@gmail.com',
             'name' => 'xyz',
             'password' => '12334567890',
         ];
 
         $response = $this->postJson(route('user.store'), $data);
 
         $response->assertStatus(201);
         $response->assertJsonStructure([
             'data' => [
                 'email',
                 'name',
                 'id',
                 'created_at',
                 'updated_at'
             ]
         ]);
     }

    /** @test */

    public function get_all_user_details_through_api()
    {
        factory(User::class, 5)->create([]);
        $response = $this->getJson(route('user.index'));
        $response->assertStatus(200);
        $this->assertCount(5, $response->decodeResponseJson()['data']);
    }

    /** @test */

    public function delete_user_through_api()
    {
        $user = factory(User::class)->create([]);
        $response = $this->deleteJson(route('user.destroy', $user->id));
        $response->assertStatus(204);
        $this->assertDatabaseMissing('users', [
            'id' => $user->id
        ]);
    }

}
